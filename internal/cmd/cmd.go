package cmd

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gcmd"
	"github.com/gogf/gf/v2/util/gtag"
)

var (
	Transfer = cTransfer{}
)

type cTransfer struct {
	g.Meta `name:"transfer" ad:"{transferAd}"`
}

const (
	transferAd = `
ADDITIONAL
    Use "transfer COMMAND -h" for details about a command.
`
)

func init() {
	gtag.Sets(g.MapStrStr{
		`transferAd`: transferAd,
	})
}

type cTransferInput struct {
	g.Meta  `name:"transfer"`
	Yes     bool `short:"y" name:"yes"     brief:"all yes for all command without prompt ask"   orphan:"true"`
	Version bool `short:"v" name:"version" brief:"show version information of current binary"   orphan:"true"`
}
type cTransferOutput struct{}

func (c cTransfer) Index(ctx context.Context, in cTransferInput) (out *cTransferOutput, err error) {
	// Version.
	if in.Version {
		_, err = Version.Index(ctx, cVersionInput{})
		return
	}
	// Print help content.
	gcmd.CommandFromCtx(ctx).Print()
	return
}
