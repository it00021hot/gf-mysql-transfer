package cmd

import (
	"context"
	"fmt"
	"gf-mysql-transfer/internal/storage"
	"github.com/gogf/gf/v2/frame/g"
)

type cStatusInput struct {
	g.Meta `name:"status"`
}
type cStatusOutput struct{}

func (c cServe) Status(ctx context.Context, in cStatusInput) (out *cStatusOutput, err error) {
	err = Initialize(ctx)
	if err != nil {
		panic(err)
	}
	doStatus()
	return nil, nil
}

func doStatus() {
	ps := storage.NewPositionStorage()
	pos, _ := ps.Get()
	fmt.Printf("The current dump position is : %s %d \n", pos.Name, pos.Pos)
}
