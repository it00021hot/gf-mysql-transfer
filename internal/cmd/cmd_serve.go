package cmd

import (
	"context"
	"gf-mysql-transfer/internal/global"
	"gf-mysql-transfer/internal/storage"
	"github.com/gogf/gf/v2/frame/g"
)

var (
	Serve = cServe{}
)

type cServe struct {
	g.Meta `short:"s" name:"serve"  brief:"run application"`
}

func Initialize(ctx context.Context) (err error) {
	// 初始化global
	err = global.Initialize(ctx)
	if err != nil {
		panic(err)
	}
	// 初始化Storage
	err = storage.Initialize()
	if err != nil {
		panic(err)
	}
	return
}
