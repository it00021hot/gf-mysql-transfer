package cmd

import (
	"context"
	"gf-mysql-transfer/internal/service"
	"github.com/gogf/gf/v2/frame/g"
)

type cStockInput struct {
	g.Meta `name:"stock"`
}
type cStockOutput struct{}

func (c cServe) Stock(ctx context.Context, in cStockInput) (out *cStockOutput, err error) {
	err = Initialize(ctx)
	if err != nil {
		panic(err)
	}
	doStock(ctx)
	return nil, nil
}

func doStock(ctx context.Context) {
	stock := service.NewStockService()
	if err := stock.Run(ctx); err != nil {
		panic(err)
	}
	stock.Close()
}
