package cmd

import (
	"context"
	"fmt"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gfile"
	"github.com/gogf/gf/v2/os/glog"
	"github.com/gogf/gf/v2/text/gstr"
)

const VERSION = "v1.0.0"

var (
	Version = cVersion{}
)

type cVersion struct {
	g.Meta `name:"version" brief:"show version information of current binary"`
}

type cVersionInput struct {
	g.Meta `name:"version"`
}
type cVersionOutput struct{}

func (c cVersion) Index(ctx context.Context, in cVersionInput) (out *cVersionOutput, err error) {
	glog.Printf(ctx, `Transfer CLI Tool %s`, VERSION)
	glog.Printf(ctx, `CLI Installed At: %s`, gfile.SelfPath())

	glog.Print(ctx, gstr.Trim(fmt.Sprintf(`
CLI Built Detail:
  Go Version:  %s
  GF Version:  %s
`, "go1.18", VERSION)))
	return
}
