package mlog

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
)

var (
	ctx    = context.TODO()
	logger = g.Log()
)

func Print(v ...interface{}) {
	logger.Info(ctx, v...)
}

func PrintF(format string, v ...interface{}) {
	logger.Infof(ctx, format, v...)
}

func Info(v ...interface{}) {
	logger.Info(ctx, v...)
}

func InfoF(format string, v ...interface{}) {
	logger.Infof(ctx, format, v...)
}

func Warning(v ...interface{}) {
	logger.Warning(ctx, v...)
}

func WarningF(format string, v ...interface{}) {
	logger.Warningf(ctx, format, v...)
}

func Debug(v ...interface{}) {
	logger.Debug(ctx, v...)
}

func Error(v ...interface{}) {
	logger.Error(ctx, v...)
}

func ErrorF(format string, v ...interface{}) {
	logger.Errorf(ctx, format, v...)
}
