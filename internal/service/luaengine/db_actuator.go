/*
 * Copyright 2020-2021 the original author(https://github.com/wj596)
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 */

package luaengine

import (
	"gf-mysql-transfer/internal/global"
	"gf-mysql-transfer/internal/model"
	"github.com/go-mysql-org/go-mysql/canal"
	"github.com/gogf/gf/v2/database/gdb"
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/util/guid"
	"github.com/yuin/gopher-lua"
)

const (
	_globalOLD = "___OLD___"
)

func dbModule(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, _dbModuleApi)
	L.Push(t)
	return 1
}

var _dbModuleApi = map[string]lua.LGFunction{
	"selectOne": selectOne,
	"select":    selectList,
	"rawRow":    rawRow,
	"oldRow":    oldRow,
	"rawAction": rawAction,

	"INSERT": dbInsert,
	"UPDATE": dbUpdate,
	"DELETE": dbDelete,
}

func oldRow(L *lua.LState) int {
	row := L.GetGlobal(_globalOLD)
	L.Push(row)
	return 1
}

func dbInsert(L *lua.LState) int {
	group := L.CheckAny(1)

	tableName := L.CheckAny(2)
	table := L.CheckAny(3)
	data := L.NewTable()
	L.SetTable(data, lua.LString("group"), group)
	L.SetTable(data, lua.LString("tableName"), tableName)
	L.SetTable(data, lua.LString("action"), lua.LString(canal.InsertAction))
	L.SetTable(data, lua.LString("table"), table)

	ret := L.GetGlobal(_globalRET)
	L.SetTable(ret, lua.LString(guid.S()), data)
	return 0
}

func dbUpdate(L *lua.LState) int {
	group := L.CheckAny(1)
	tableName := L.CheckAny(2)
	where := L.CheckAny(3)
	table := L.CheckAny(4)

	data := L.NewTable()
	L.SetTable(data, lua.LString("group"), group)
	L.SetTable(data, lua.LString("tableName"), tableName)
	L.SetTable(data, lua.LString("action"), lua.LString(canal.UpdateAction))
	L.SetTable(data, lua.LString("where"), where)
	L.SetTable(data, lua.LString("table"), table)

	ret := L.GetGlobal(_globalRET)
	L.SetTable(ret, lua.LString(guid.S()), data)
	return 0
}

func dbDelete(L *lua.LState) int {
	group := L.CheckAny(1)
	tableName := L.CheckAny(2)
	where := L.CheckAny(3)

	data := L.NewTable()
	L.SetTable(data, lua.LString("group"), group)
	L.SetTable(data, lua.LString("tableName"), tableName)
	L.SetTable(data, lua.LString("action"), lua.LString(canal.DeleteAction))
	L.SetTable(data, lua.LString("where"), where)

	ret := L.GetGlobal(_globalRET)
	L.SetTable(ret, lua.LString(guid.S()), data)
	return 0
}

func DoDbOps(input map[string]interface{}, oldMap map[string]interface{}, action string, rule *global.Rule) ([]*model.DbRespond, error) {
	L := _pool.Get()
	defer _pool.Put(L)

	row := L.NewTable()
	paddingTable(L, row, input)
	old := L.NewTable()
	paddingTable(L, old, oldMap)
	ret := L.NewTable()
	L.SetGlobal(_globalRET, ret)
	L.SetGlobal(_globalROW, row)
	L.SetGlobal(_globalOLD, old)
	L.SetGlobal(_globalACT, lua.LString(action))

	funcFromProto := L.NewFunctionFromProto(rule.LuaProto)
	L.Push(funcFromProto)
	err := L.PCall(0, lua.MultRet, nil)
	if err != nil {
		return nil, err
	}

	asserted := true
	responds := make([]*model.DbRespond, 0, ret.Len())
	ret.ForEach(func(k lua.LValue, v lua.LValue) {
		resp := new(model.DbRespond)
		if lvToString(L.GetTable(v, lua.LString("group"))) == "" {
			resp.Group = gdb.DefaultGroupName
		} else {
			resp.Group = lvToString(L.GetTable(v, lua.LString("group")))
		}
		resp.TableName = lvToString(L.GetTable(v, lua.LString("tableName")))
		resp.Action = lvToString(L.GetTable(v, lua.LString("action")))
		lvWhere := L.GetTable(v, lua.LString("where"))
		lvTable := L.GetTable(v, lua.LString("table"))

		var table map[string]interface{}
		var where map[string]interface{}
		if action != canal.DeleteAction {
			table, asserted = lvToMap(lvTable)
			if !asserted {
				return
			}
			resp.Table = table
		}
		if action != canal.InsertAction {
			where, asserted = lvToMap(lvWhere)
			if !asserted {
				return
			}
			resp.Where = where
		}

		responds = append(responds, resp)
	})

	if !asserted {
		return nil, gerror.New("The parameter must be of table type")
	}

	return responds, nil
}
