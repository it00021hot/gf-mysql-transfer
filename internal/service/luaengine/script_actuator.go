/*
 * Copyright 2020-2021 the original author(https://github.com/wj596)
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 */

package luaengine

import (
	lua "github.com/yuin/gopher-lua"

	"gf-mysql-transfer/internal/global"
)

func scriptModule(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, _scriptModuleApi)
	L.Push(t)
	return 1
}

var _scriptModuleApi = map[string]lua.LGFunction{
	"selectOne": selectOne,
	"select":    selectList,
	"rawRow":    rawRow,
	"oldRow":    oldRow,
	"rawAction": rawAction,
}

func DoScript(input map[string]interface{}, action string, rule *global.Rule) error {
	L := _pool.Get()
	defer _pool.Put(L)

	row := L.NewTable()
	paddingTable(L, row, input)

	L.SetGlobal(_globalROW, row)
	L.SetGlobal(_globalACT, lua.LString(action))

	funcFromProto := L.NewFunctionFromProto(rule.LuaProto)
	L.Push(funcFromProto)
	err := L.PCall(0, lua.MultRet, nil)
	if err != nil {
		return err
	}

	return nil
}
