/*
 * Copyright 2020-2021 the original author(https://github.com/wj596)
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 */

package luaengine

import (
	"gf-mysql-transfer/internal/utility/mlog"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/yuin/gopher-lua"
)

var (
	ctx = gctx.New()
)

func httpModule(L *lua.LState) int {
	t := L.NewTable()
	L.SetFuncs(t, _httpModuleApi)
	L.Push(t)
	return 1
}

var _httpModuleApi = map[string]lua.LGFunction{
	"selectOne": selectOne,
	"select":    selectList,
	"rawRow":    rawRow,
	"oldRow":    oldRow,
	"rawAction": rawAction,

	"get":    doGet,
	"delete": doDelete,
	"post":   doPost,
	"put":    doPut,
}

func doGet(L *lua.LState) int {
	ret := L.NewTable()
	paramUrl := L.CheckString(1)
	paramOps := L.CheckTable(2)

	if headers, ok := lvToMap(paramOps); ok {
		_httpClient.SetHeaderMap(gconv.MapStrStr(headers))
	}

	entity, err := _httpClient.Get(ctx, paramUrl)
	if err != nil {
		mlog.Error(err.Error())
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}

	ret.RawSet(lua.LString("status_code"), lua.LNumber(entity.StatusCode))
	ret.RawSet(lua.LString("body"), lua.LString(entity.ReadAllString()))

	L.Push(ret)
	return 1
}

func doDelete(L *lua.LState) int {
	ret := L.NewTable()
	paramUrl := L.CheckString(1)
	paramOps := L.CheckTable(2)

	if headers, ok := lvToMap(paramOps); ok {
		_httpClient.SetHeaderMap(gconv.MapStrStr(headers))
	}

	entity, err := _httpClient.Delete(ctx, paramUrl)
	if err != nil {
		mlog.Error(err.Error())
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}

	ret.RawSet(lua.LString("status_code"), lua.LNumber(entity.StatusCode))
	ret.RawSet(lua.LString("body"), lua.LString(entity.ReadAllString()))

	L.Push(ret)
	return 1
}

func doPost(L *lua.LState) int {
	ret := L.NewTable()
	paramUrl := L.CheckString(1)
	paramHeaders := L.CheckTable(2)
	paramContents := L.CheckTable(3)

	cli := _httpClient
	if headers, ok := lvToMap(paramHeaders); ok {
		cli.SetHeaderMap(gconv.MapStrStr(headers))
	}

	contents, ok := lvToMap(paramContents)
	if !ok {
		mlog.Error("The argument must Table")
		L.Push(lua.LNil)
		L.Push(lua.LString("The argument must Table"))
		return 2
	}

	entity, err := cli.Post(ctx, paramUrl, contents)
	if err != nil {
		mlog.Error(err.Error())
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}

	ret.RawSet(lua.LString("status_code"), lua.LNumber(entity.StatusCode))
	ret.RawSet(lua.LString("body"), lua.LString(entity.ReadAllString()))

	L.Push(ret)
	return 1
}

func doPut(L *lua.LState) int {
	ret := L.NewTable()
	paramUrl := L.CheckString(1)
	paramHeaders := L.CheckTable(2)
	paramContents := L.CheckTable(3)

	cli := _httpClient
	if headers, ok := lvToMap(paramHeaders); ok {
		cli.SetHeaderMap(gconv.MapStrStr(headers))
	}

	contents, ok := lvToMap(paramContents)
	if !ok {
		mlog.Error("The argument must Table")
		L.Push(lua.LNil)
		L.Push(lua.LString("The argument must Table"))
		return 2
	}

	entity, err := cli.Put(ctx, paramUrl, contents)
	if err != nil {
		mlog.Error(err.Error())
		L.Push(lua.LNil)
		L.Push(lua.LString(err.Error()))
		return 2
	}

	ret.RawSet(lua.LString("status_code"), lua.LNumber(entity.StatusCode))
	ret.RawSet(lua.LString("body"), lua.LString(entity.ReadAllString()))

	L.Push(ret)
	return 1
}
