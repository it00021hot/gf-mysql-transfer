package log

import (
	"context"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/glog"
	"github.com/gogf/gf/v2/text/gstr"
)

var logHandler glog.Handler = func(ctx context.Context, in *glog.HandlerInput) {
	//按日志级别存储日志文件
	in.Logger.SetFile(gstr.Trim(in.LevelFormat, "[]") + "-{Y-m-d}.log")
	in.Next(ctx)
}

func InitLog() {
	g.Log().SetLevelPrefixes(map[int]string{
		glog.LEVEL_DEBU: "DEBUG",
		glog.LEVEL_INFO: "INFO",
		glog.LEVEL_WARN: "WARN",
		glog.LEVEL_ERRO: "ERROR",
	})
	g.Log().SetHandlers(logHandler)

}
