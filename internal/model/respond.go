/*
 * Copyright 2020-2021 the original author(https://github.com/wj596)
 *
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 */

package model

import "sync"

var mqRespondPool = sync.Pool{
	New: func() interface{} {
		return new(MQRespond)
	},
}

var esRespondPool = sync.Pool{
	New: func() interface{} {
		return new(ESRespond)
	},
}

var mongoRespondPool = sync.Pool{
	New: func() interface{} {
		return new(MongoRespond)
	},
}

var redisRespondPool = sync.Pool{
	New: func() interface{} {
		return new(RedisRespond)
	},
}

type MQRespond struct {
	Topic     string      `json:"-"`
	Action    string      `json:"action"`
	Timestamp uint32      `json:"timestamp"`
	Raw       interface{} `json:"raw,omitempty"`
	Date      interface{} `json:"date"`
	ByteArray []byte      `json:"-"`
}

type ESRespond struct {
	Index  string
	Id     string
	Action string
	Date   string
}

type MongoRespond struct {
	RuleKey    string
	Collection string
	Action     string
	Id         interface{}
	Table      map[string]interface{}
}

type DbRespond struct {
	Group     string
	RuleKey   string
	TableName string
	Action    string
	Where     map[string]interface{}
	Table     map[string]interface{}
}

type RedisRespond struct {
	Action    string
	Structure string
	Key       string
	Field     string
	Score     float64
	OldVal    interface{}
	Val       interface{}
}

func BuildMQRespond() *MQRespond {
	return mqRespondPool.Get().(*MQRespond)
}

func ReleaseMQRespond(t *MQRespond) {
	mqRespondPool.Put(t)
}

func BuildESRespond() *ESRespond {
	return esRespondPool.Get().(*ESRespond)
}

func ReleaseESRespond(t *ESRespond) {
	esRespondPool.Put(t)
}

func BuildMongoRespond() *MongoRespond {
	return mongoRespondPool.Get().(*MongoRespond)
}

func ReleaseMongoRespond(t *MongoRespond) {
	mongoRespondPool.Put(t)
}

func BuildRedisRespond() *RedisRespond {
	return redisRespondPool.Get().(*RedisRespond)
}

func ReleaseRedisRespond(t *RedisRespond) {
	redisRespondPool.Put(t)
}
