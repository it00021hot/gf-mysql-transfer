package v1

import (
	"github.com/gogf/gf/v2/frame/g"
)

type MonitorReq struct {
	g.Meta `path:"/monitor" tags:"index" method:"get" summary:"获取节点信息"`
}

type MonitorRes struct {
	Mysql         string            `json:"mysql" dc:"mysql地址(服务端)"`
	BinName       string            `json:"binName" dc:"当前同步binlog文件名"`
	BinPos        uint32            `json:"binPos" dc:"当前binlog位置"`
	DestName      string            `json:"destName" dc:"目标端类型名称"`
	DestAddr      string            `json:"destAddr" dc:"目标端地址"`
	DestState     bool              `json:"destState" dc:"目标端状态"`
	BootTime      string            `json:"bootTime" dc:"启动时间"`
	InsertAmount  uint64            `json:"insertAmount" dc:"新增数量"`
	UpdateAmount  uint64            `json:"updateAmount" dc:"更新数量"`
	DeleteAmount  uint64            `json:"deleteAmount" dc:"删除数量"`
	Tables        []string          `json:"tables" dc:"规则表名称数组"`
	InsertAmounts map[string]uint64 `json:"insertAmounts" dc:"规则表新增详情"`
	UpdateAmounts map[string]uint64 `json:"updateAmounts" dc:"规则表修改详情"`
	DeleteAmounts map[string]uint64 `json:"deleteAmounts" dc:"规则表删除详情"`
	IsCluster     bool              `json:"isCluster" dc:"是否集群"`
	IsLeader      bool              `json:"isLeader" dc:"是否主节点"`
	IsZk          bool              `json:"isZk" dc:"是否注册到zk"`
	ZkAddress     string            `json:"zkAddress" dc:"zk地址"`
	IsEtcd        bool              `json:"isEtcd" dc:"是否注册到etcd"`
	EtcdAddress   string            `json:"etcdAddress" dc:"etcd地址"`
	Leader        string            `json:"leader" dc:"leader节点地址"`
	Followers     []string          `json:"followers" dc:"节点地址"`
}

type PingReq struct {
	g.Meta `path:"/ping" tags:"index" method:"get" summary:"检测连接是否可用"`
}
type PingRes struct {
}

type PositionReq struct {
	g.Meta `path:"/position" tags:"index" method:"post" summary:"更新position"`
	Name   string `json:"name" v:"required#binlog文件名不能为空" dc:"binlog文件名"`
	Pos    uint32 `json:"pos" v:"min:4#同步位置不能小于4" dc:"binlog同步位置"`
}

type PositionRes struct {
}
