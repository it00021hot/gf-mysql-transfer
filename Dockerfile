FROM golang:1.19.2-alpine3.16 as compiler

ENV GO111MODULE=on \
    GOPROXY=https://goproxy.cn,direct

WORKDIR /app

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o transfer .

RUN mkdir publish && cp transfer publish && cp -r config publish && cp -r lua publish
#RUN cp -r resource publish

# 第二阶段
FROM alpine:3.16

WORKDIR /app

COPY --from=compiler /app/publish .
RUN chmod +xr ./transfer

# 暴露端口
EXPOSE 8060

ENTRYPOINT []
CMD ./transfer serve start