# 相关配置如下：

```yaml
#规则配置
rule:
  - schema: test #数据库名称
    table: '*' #表名称 *匹配数据库下所有表
    order_by_column: id #排序字段，不指定则取pk
  - schema: test #数据库名称
    table: test #表名称
    order_by_column: id #排序字段，不指定则取pk
    #column_lower_case:false #列名称转为小写,默认为false
    #column_upper_case:false#列名称转为大写,默认为false
    #column_underscore_to_camel: true #列名称下划线转驼峰,默认为false
    # 包含的列，多值逗号分隔，如：id,name,age,area_id  为空时表示包含全部列
    #include_columns: ID,USER_NAME,PASSWORD
    #exclude_columns: BIRTHDAY,MOBIE # 排除掉的列，多值逗号分隔，如：id,name,age,area_id  默认为空
    #column_mappings: USER_NAME=account    #列名称映射，多个映射关系用逗号分隔，如：USER_NAME=account 表示将字段名USER_NAME映射为account
    #default_column_value_config: sync_complete=1  #默认的列-值，多个用逗号分隔，如：source=binlog,area_name=合肥
    #date_formatter: yyyy-MM-dd #date类型格式化， 不填写默认yyyy-MM-dd
    #datetime_formatter: yyyy-MM-dd HH:mm:ss #datetime、timestamp类型格式化，不填写默认yyyy-MM-dd HH:mm:ss
    #lua_file_path: lua/test.lua   #lua脚本文件
    #lua_script:   #lua 脚本
    #value_encoder: json  #值编码，支持json、kv-commas、v-commas；默认为json
    #value_formatter: '{{.ID}}|{{.USER_NAME}}' # 值格式化表达式，如：{{.ID}}|{{.USER_NAME}},{{.ID}}表示ID字段的值、{{.USER_NAME}}表示USER_NAME字段的值
    #db相关
    target_group: default #目标db分组名称(默认为default)
    target_table: test #目标db表名称(默认与表名称相同)
```
# 示例
test表，数据如下：
![source.png](img/source.png)
### 示例一
使用上述配置
同步到Mysql的数据如下:
![target.png](img/target.png)
### 示例二
配置如下：
```yaml
#规则配置
rule:
  - schema: test #数据库名称
    table: test #表名称
    default_column_value_config: sync_complete=1  #默认的列-值，多个用逗号分隔，如：source=binlog,area_name=合肥
    #db相关
    target_group: default #目标db分组名称(默认为default)
    target_table: test_1 #目标db表名称(默认与表名称相同)
```
test_1表结构如下：
```mysql
create table test_1
(
    id            bigint auto_increment comment '编号'
        primary key,
    user_name     varchar(20)          null comment '用户姓名',
    user_age      int                  null comment '用户年龄',
    sync_complete tinyint(1) default 0 null comment '同步标识'
)
    comment '测试表';
```
其中default_column_value_config表示对列名称进行映射
同步到test_1的数据如下:
![test_1.png](img/test_1.png)
可以看到同步的sync_complete字段值为1，而非默认值0