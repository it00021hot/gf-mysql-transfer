# 相关配置如下：

```yaml
system:
  # mysql配置
  addr: 127.0.0.1:3306
  user: root
  pass: root
  charset: utf8mb4
  slave_id: 1001 #slave ID
  flavor: mysql #mysql or mariadb,默认mysql

  refresh_rule: false #是否动态刷新rule
  enabled_pool: true #是否启动协程池，当目标类型是数据库且同步表存在外键索引，请勿开启
  #目标类型
  target: db # 支持redis、mongodb、elasticsearch、rocketmq、kafka、rabbitmq、db
  # db连接配置
  db:
    default:  # db分组名称
      debug: true #开启调试模式
      host: "127.0.0.1"
      port: "3306"
      user: "root"
      pass: "root"
      name: "gen_code_test"
      type: "mysql" # db类型 mysql、mssql、pgsql
    test:
      debug: true
      host: "127.0.0.1"
      port: "3306"
      user: "root"
      pass: "root"
      name: "gen_code_test"
      type: "mysql"
```
更多db相关参数见：[gf官网](https://goframe.org/pages/viewpage.action?pageId=1114245)
- [基于规则同步](./mysql_rule.md)
- [基于Lua脚本同步](./mysql_lua.md)