# 使用Lua脚本可以实现更复杂的数据处理逻辑，gf-mysql-transfer支持Lua5.1语法

# 示例
test表，数据如下：
![source.png](img/source.png)
### 示例一
Lua脚本：
```lua
local ops = require("dbOps") --加载db操作模块

local group = "default"  --目标分组名称
local tableName = "test_1" --目标表名称
local row = ops.rawRow()  --当前数据库的一行数据,table类型
local action = ops.rawAction()  --当前数据库事件,包括：insert、update、delete

local result = {}  -- 定义一个table
local where = {} --更新或删除条件
for k, v in pairs(row) do
    result[k] = v
end

where["id"] = row["id"] --获取ID列的值
if action == "insert" then
    -- 新增事件
    result["sync_complete"] = 1 --新增时，设置sync_complete=1
    ops.INSERT(group, tableName, result) -- 新增，参数group为目标分组，字符串类型；参数tableName为目标表，字符串类型；参数result为要插入的数据，table类型
elseif action == "delete" then
    -- 删除事件  -- 修改事件
    ops.DELETE(group, tableName, where) -- 删除，参数group为目标分组，字符串类型；参数tableName为目标表，字符串类型；参数where为修改条件，table类型
else
    -- 修改事件
    ops.UPDATE(group, tableName, where, result) -- 修改，参数group为目标分组，字符串类型；参数tableName为目标表，字符串类型；参数where为修改条件，table类型；参数result为要插入的数据，table类型
end
```
引入Lua脚本：
```yaml
#规则配置
rule:
  - schema: test #数据库名称
    table: test #表名称
    lua_file_path: lua/test.lua   #lua脚本文件
```
使用上述配置
同步到Mysql的数据如下:
![target.png](img/target.png)

### dbOps模块
提供的方法如下：
1. INSERT: 插入操作,如：ops.INSERT(group, tableName, result)。参数group为目标分组，字符串类型；tableName为目标表，字符串类型；参数result为要插入的数据，table类型
2. UPDATE: 修改操作,如：ops.UPDATE(group, tableName, where, result)。参数group为目标分组，字符串类型；参数tableName为目标表，字符串类型；参数where为修改条件，table类型；参数result为要插入的数据，table类型
3. DELETE: 删除操作,如：ops.DELETE(group, tableName, where)。参数group为目标分组，字符串类型；参数tableName为目标表，字符串类型；参数where为修改条件，table类型
